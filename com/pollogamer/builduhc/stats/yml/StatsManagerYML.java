package com.pollogamer.builduhc.stats.yml;

import com.pollogamer.builduhc.Principal;
import org.bukkit.entity.Player;

import java.util.UUID;

public class StatsManagerYML {

    private static Principal plugin;

    public StatsManagerYML(Principal plugin){
        this.plugin = plugin;
    }

    public static void addWin(Player p){
        UUID puuid = p.getUniqueId();
        int oldWins = plugin.getStats().getInt("players."+puuid+".wins");
        int newWins = oldWins+1;
        plugin.getStats().set("players."+puuid+".wins",newWins);
    }
    public static int getWins(Player p){
        UUID puuid = p.getUniqueId();
        int wins = plugin.getStats().getInt("players."+puuid+".wins");
        return wins;
    }
    public static void addLoses(Player p){
        UUID puuid = p.getUniqueId();
        int olddeaths = plugin.getStats().getInt("players."+puuid+".loses");
        int newdeaths = olddeaths+1;
        plugin.getStats().set("players."+puuid+".loses", newdeaths);
    }
    public static int getLoses(Player p){
        UUID puuid = p.getUniqueId();
        int deaths = plugin.getStats().getInt("players."+puuid+".loses");
        return deaths;
    }
    public static void addPlayed(Player p){
        UUID puuid = p.getUniqueId();
        int oldplayed = plugin.getStats().getInt("players."+puuid+".played");
        int newplayed = oldplayed+1;
        plugin.getStats().set("players."+puuid+".played", newplayed);
    }
    public static int getPlayed(Player p){
        UUID puuid = p.getUniqueId();
        int played = plugin.getStats().getInt("players."+puuid+".played");
        return played;
    }
}
