package com.pollogamer.builduhc.stats;

import com.pollogamer.builduhc.stats.yml.StatsManagerYML;
import com.pollogamer.builduhc.utils.Lang;
import org.bukkit.entity.Player;

public class StatsManager {

    public static void addWin(Player p){
        if(Lang.statsyml){
            StatsManagerYML.addWin(p);
        }else{

        }
    }
    public static int getWins(Player p){
        if(Lang.statsyml){
            return StatsManagerYML.getWins(p);
        }else{

        }
        return 0;
    }
    public static void addLoses(Player p){
        if(Lang.statsyml){
            StatsManagerYML.addLoses(p);
        }else{

        }
    }
    public static int getLoses(Player p){
        if(Lang.statsyml){
            return StatsManagerYML.getLoses(p);
        }else{

        }
        return 0;
    }
    public static void addPlayed(Player p){
        if(Lang.statsyml){
            StatsManagerYML.addPlayed(p);
        }else{

        }
    }
    public static int getPlayed(Player p){
        if(Lang.statsyml){
            return StatsManagerYML.getPlayed(p);
        }else{

        }
        return 0;
    }
}
