package com.pollogamer.builduhc.manager;

import com.pollogamer.builduhc.Principal;
import com.pollogamer.builduhc.arena.Arena;
import com.pollogamer.builduhc.arena.ArenaManager;
import com.pollogamer.builduhc.stats.StatsManager;
import com.pollogamer.builduhc.stats.yml.StatsManagerYML;
import com.pollogamer.builduhc.utils.Lang;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;
import java.util.Iterator;

public class ScoreboardMan {

    private Principal plugin;

    public ScoreboardMan(Principal plugin){
        this.plugin = plugin;
    }


    public void setScoreboardLobby(Player player){
        ScoreboardManager manager = Bukkit.getScoreboardManager();
        Scoreboard board = manager.getNewScoreboard();
        Objective objective = board.registerNewObjective("Scoreboard","dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        int scoretoset = Lang.scoreboardlobbylines.size();
        Iterator iterator = Lang.scoreboardlobbylines.iterator();
        while(iterator.hasNext()){
            String string = (String)iterator.next();
            string = string.replaceAll("%player%",player.getName());
            string = string.replaceAll("%online%", Bukkit.getOnlinePlayers().size()+"");
            string = string.replaceAll("%kills%", StatsManager.getWins(player)+"");
            string = string.replaceAll("%deaths%", StatsManager.getLoses(player)+"");
            string = string.replaceAll("%played%", StatsManager.getPlayed(player)+"");
            Score score = objective.getScore(string);
            score.setScore(scoretoset);
            --scoretoset;
        }
        objective.setDisplayName(Lang.scoreboardlobbyname);
        player.setScoreboard(board);
    }
    public void setScoreboardGame(Player player, Arena arena){
        ScoreboardManager manager = Bukkit.getScoreboardManager();
        Scoreboard board = manager.getNewScoreboard();
        Objective objective = board.registerNewObjective("Scoreboard","dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        int scoretoset = Lang.scoreboardgamelines.size();
        Iterator iterator = Lang.scoreboardgamelines.iterator();
        while(iterator.hasNext()){
            String string = (String)iterator.next();
            string = string.replaceAll("%player%",player.getName());
            string = string.replaceAll("%arena%", arena.getName());
            string = string.replaceAll("%online%", Bukkit.getOnlinePlayers().size()+"");
            string = string.replaceAll("%kills%", StatsManager.getWins(player)+"");
            string = string.replaceAll("%deaths%", StatsManager.getLoses(player)+"");
            string = string.replaceAll("%played%", StatsManager.getPlayed(player)+"");
            string = string.replaceAll("%status%", ArenaManager.getManager().estado(arena));
            Score score = objective.getScore(string);
            score.setScore(scoretoset);
            --scoretoset;
        }
        objective.setDisplayName(Lang.scoreboardgamename);
        player.setScoreboard(board);
    }
}
