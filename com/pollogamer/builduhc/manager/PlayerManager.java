package com.pollogamer.builduhc.manager;

import com.minebone.itemstack.ItemStackBuilder;
import com.pollogamer.builduhc.arena.ArenaManager;
import com.pollogamer.builduhc.utils.Items;
import com.pollogamer.builduhc.utils.Lang;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class PlayerManager {

    public static void setbuildffakit(Player p){
        p.getInventory().clear();
            p.getInventory().addItem(new ItemStackBuilder(Material.DIAMOND_SWORD));
            p.getInventory().addItem(new ItemStackBuilder(Material.FISHING_ROD));
            p.getInventory().addItem(new ItemStackBuilder(Material.BOW));
            p.getInventory().addItem(new ItemStackBuilder(Material.COOKED_BEEF).setStackAmount(64));
            p.getInventory().addItem(new ItemStackBuilder(Material.FLINT_AND_STEEL));
            p.getInventory().addItem(new ItemStackBuilder(Material.WOOD).setStackAmount(64));
            p.getInventory().addItem(new ItemStackBuilder(Material.GOLDEN_APPLE));
            //p.getInventory().addItem(goldenhead);
            p.getInventory().setItem(9,new ItemStackBuilder(Material.ARROW).setStackAmount(64));
            p.getInventory().setItem(10,new ItemStackBuilder(Material.COBBLESTONE).setStackAmount(64));
        p.getInventory().setArmorContents(null);
        p.getInventory().setHelmet(new ItemStackBuilder(Material.DIAMOND_HELMET));
        p.getInventory().setChestplate(new ItemStackBuilder(Material.DIAMOND_CHESTPLATE));
        p.getInventory().setLeggings(new ItemStackBuilder(Material.DIAMOND_LEGGINGS));
        p.getInventory().setBoots(new ItemStackBuilder(Material.DIAMOND_BOOTS));
    }

    public static void putallplayer(Player p){
        ArenaManager.getManager().locs.put(p.getName(), p.getLocation());
        ArenaManager.getManager().inv.put(p.getName(), p.getInventory().getContents());
        ArenaManager.getManager().armor.put(p.getName(), p.getInventory().getArmorContents());
        p.setMaxHealth(20);
        p.setHealth(20);
        p.setFireTicks(0);
        p.getInventory().setArmorContents(null);
        p.getInventory().clear();
        p.getInventory().setItem(Lang.itemleaveslot, Items.leave);
    }

    public static void backallplayer(Player p){

    }
}
