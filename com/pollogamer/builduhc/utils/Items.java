package com.pollogamer.builduhc.utils;

import com.minebone.itemstack.ItemStackBuilder;
import com.pollogamer.builduhc.Principal;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.Random;

public class Items {

    private Principal plugin;
    public static ItemStackBuilder leave;

    public Items(Principal plugin){
        this.plugin = plugin;
        leave = new ItemStackBuilder(Material.getMaterial(Lang.itemleaveid)).setStackData((short)Lang.itemleavedata).setStackAmount(Lang.itemleaveamount).setName(Lang.itemleavename);
    }

    public static void spawnFirework(Location loc){
        Firework fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();
        Random r = new Random();
        int rt = r.nextInt(3) + 1;
        FireworkEffect.Type type = FireworkEffect.Type.BALL;
        if (rt == 1) type = FireworkEffect.Type.BALL;
        if (rt == 2) type = FireworkEffect.Type.BALL_LARGE;
        if (rt == 3) type = FireworkEffect.Type.BURST;
        if (rt == 4) type = FireworkEffect.Type.STAR;
        Color c1 = Color.ORANGE;
        Color c2 = Color.YELLOW;
        Color c3 = Color.GREEN;
        FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).withColor(c1).withFade(c2).with(type).trail(r.nextBoolean()).build();
        fwm.addEffect(effect);
        int rp = 0;
        fwm.setPower(rp);
        fw.setFireworkMeta(fwm);
    }

}
