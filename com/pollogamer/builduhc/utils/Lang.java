package com.pollogamer.builduhc.utils;

import com.pollogamer.builduhc.Principal;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

public class Lang{

    private Principal plugin;

    public Lang(Principal plugin){
        this.plugin = plugin;
        init();
    }

    public static boolean joinlobby;
    public static boolean scoreboardinlobby;
    public static boolean scoreboardingame;
    public static boolean fireworks;
    public static boolean statsyml;
    public static int scoreboardrefresh;
    public static int cooldown;
    public static int endingcountdown;
    public static int statsinterval;
    public static int itemleaveslot;
    public static int itemleaveid;
    public static int itemleavedata;
    public static int itemleaveamount;
    public static String breakblock;
    public static String craftingame;
    public static String itemleavename;
    public static String nominplayers;
    public static String arenastarting;
    public static String playerjoin;
    public static String playerleave;
    public static String alreadyingame;
    public static String noingame;
    public static String arenafull;
    public static String commandsingame;
    public static String guiarenatitle;
    public static String guiarenanameitem;
    public static String guiarenastatewaiting;
    public static String guiarenastatestarting;
    public static String guiarenastateingame;
    public static String scoreboardlobbyname;
    public static String scoreboardgamename;
    public static String joinarena;
    public static String leftarena;
    public static List<String> guiarenalore = new ArrayList<String>();
    public static List<String> arenastarted = new ArrayList<String>();
    public static List<String> arenaend = new ArrayList<String>();
    public static List<String> scoreboardlobbylines = new ArrayList<String>();
    public static List<String> scoreboardgamelines= new ArrayList<String>();

    public void init(){
        guiarenalore.clear();
        arenastarted.clear();
        arenaend.clear();
        scoreboardlobbylines.clear();
        scoreboardgamelines.clear();
        joinlobby = plugin.getConfig().getBoolean("joinlobby");
        scoreboardingame = plugin.getConfig().getBoolean("scoreboard.ingame.enable");
        fireworks =  plugin.getConfig().getBoolean("fireworks-on-end");
        scoreboardinlobby = plugin.getConfig().getBoolean("scoreboard.inlobby.enable");
        cooldown = plugin.config.getInt("countdown")+1;
        endingcountdown = plugin.config.getInt("endingcountdown");
        scoreboardrefresh = plugin.getConfig().getInt("scoreboard.refresh");
        statsinterval = plugin.getConfig().getInt("save-stats-interval");
        itemleaveslot = plugin.getConfig().getInt("items.ingamelobby.leave.slot");
        itemleaveid = plugin.getConfig().getInt("items.ingamelobby.leave.id");
        itemleavedata = plugin.getConfig().getInt("items.ingamelobby.leave.data");
        itemleaveamount = plugin.getConfig().getInt("items.ingamelobby.leave.amount");
        breakblock = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("messages.arena-no-break"));
        craftingame = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("messages.craft-in-arena"));
        nominplayers = ChatColor.translateAlternateColorCodes('&',plugin.config.getString("messages.no-min-players"));
        arenastarting = ChatColor.translateAlternateColorCodes('&',plugin.config.getString("messages.arena-starting"));
        playerjoin = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("messages.arena-join"));
        playerleave = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("messages.arena-leave"));
        alreadyingame = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("messages.already-in-game"));
        noingame = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("messages.no-in-game"));
        arenafull = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("messages.arena-full"));
        itemleavename = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("items.ingamelobby.leave.name"));
        commandsingame = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("messages.commands-in-game"));
        guiarenatitle = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("gui.arenas.title"));
        guiarenanameitem = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("gui.arenas.nameitem"));
        guiarenastatewaiting = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("gui.arenas.state.waiting"));
        guiarenastatestarting = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("gui.arenas.state.starting"));
        guiarenastateingame = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("gui.arenas.state.ingame"));
        scoreboardlobbyname = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("scoreboard.inlobby.title"));
        scoreboardgamename = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("scoreboard.ingame.title"));
        joinarena = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("messages.joined-the-arena"));
        leftarena = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("messages.left-the-arena"));

        for(String s : plugin.config.getStringList("messages.arena-started")){
            s = s.replaceAll("&","§");
            arenastarted.add(s);
        }
        for(String s : plugin.config.getStringList("messages.arena-end")){
            s = s.replaceAll("&","§");
            arenaend.add(s);
        }
        for(String s : plugin.getConfig().getStringList("gui.arenas.lore")){
            s = s.replaceAll("&","§");
            guiarenalore.add(s);
        }
        for(String s : plugin.getConfig().getStringList("scoreboard.inlobby.lines")){
            s = s.replaceAll("&","§");
            scoreboardlobbylines.add(s);
        }
        for(String s : plugin.getConfig().getStringList("scoreboard.ingame.lines")){
            s = s.replaceAll("&","§");
            scoreboardgamelines.add(s);
        }
    }
}
