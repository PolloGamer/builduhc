package com.pollogamer.builduhc.utils;

import com.pollogamer.builduhc.Principal;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ConfigUtils{

    private static Principal plugin;

    public ConfigUtils(Principal plugin){
        this.plugin = plugin;
    }

    public static void loadConfigsFile(){
        if (!plugin.getDataFolder().exists()){
            plugin.getDataFolder().mkdir();
        }

        File cfile = new File(plugin.getDataFolder(),"config.yml");
        File mfiles = new File(plugin.getDataFolder(),"maps.yml");
        File statsfile = new File(plugin.getDataFolder(),"stats.yml");

        if(!cfile.exists()){
            plugin.saveResource("config.yml",false);
            plugin.getLogger().info("Config file created!");
        }
        if(!mfiles.exists()){
            plugin.saveResource("maps.yml",false);
            plugin.getLogger().info("Maps file created!");
        }
        if(!statsfile.exists()){
            plugin.saveResource("stats.yml",false);
            plugin.getLogger().info("Stats file created!");
        }

        plugin.config = new YamlConfiguration();
        plugin.maps = new YamlConfiguration();
        plugin.stats = new YamlConfiguration();

        try {
            plugin.config.load(cfile);
            plugin.getLogger().info("Config file loaded!");
            plugin.maps.load(mfiles);
            plugin.getLogger().info("Maps file loaded!");
            plugin.stats.load(statsfile);
            plugin.getLogger().info("Stats file loaded");
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveMapsfile(){
        File mfile = new File(plugin.getDataFolder(),"maps.yml");
        try {
            plugin.maps.save(mfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveStatsfile(){
        new BukkitRunnable(){
            public void run(){
                try {
                    File sfile = new File(plugin.getDataFolder(),"stats.yml");
                    plugin.stats.save(sfile);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }.runTaskTimer(plugin,0,20*Lang.statsinterval);
    }
}
