package com.pollogamer.builduhc.gui;

import com.minebone.gui.inventory.GUIPage;
import com.minebone.gui.inventory.button.SimpleButton;
import com.minebone.itemstack.ItemStackBuilder;
import com.pollogamer.builduhc.Principal;
import com.pollogamer.builduhc.arena.Arena;
import com.pollogamer.builduhc.arena.ArenaManager;
import com.pollogamer.builduhc.utils.Lang;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;

public class ArenasMenu extends GUIPage{

    private BukkitTask runnable;

    public ArenasMenu(Principal plugin, Player player) {
        super(plugin, player, Lang.guiarenatitle, ((int) Math.ceil(ArenaManager.getManager().arenas.size() / 9.0))*9);
        build();
        runnable = new BukkitRunnable(){
            public void run(){
                refresh();
            }
        }.runTaskTimer(plugin,0,20);
    }

    @Override
    public void buildPage() {
        int slot = -1;
        for(Arena a : ArenaManager.getManager().arenas){
            slot = slot+1;
            addButton(new SimpleButton(setItemStack(a)).setAction((plugin,player,page) -> ArenaManager.getManager().addPlayer(player,a.getName())),slot);
        }
    }

    public ItemStackBuilder setItemStack(Arena a){
        int playersingame = a.getPlayers().size();
        int maxplayers = a.maxplayers;
        String name = Lang.guiarenanameitem.replaceAll("%arena%",a.getName());
        List<String> lore = new ArrayList<String>();
        for(String s : Lang.guiarenalore){
            s = s.replaceAll("%players%",playersingame+"");
            s = s.replaceAll("%maxplayers%",maxplayers+"");
            s = s.replaceAll("%state%",ArenaManager.getManager().estado(a));
            lore.add(s);
        }

        if(a.estado == Arena.Estado.Empezando){
            return new ItemStackBuilder(Material.WOOL).setStackData((short)2).setName(name).setLore(lore).setStackAmount(playersingame);
        }else if(a.estado == Arena.Estado.EnJuego){
            return new ItemStackBuilder(Material.WOOL).setStackData((short)14).setName(name).setLore(lore).setStackAmount(playersingame);
        }else if(a.estado == Arena.Estado.Esperando){
            return new ItemStackBuilder(Material.WOOL).setStackData((short)5).setName(name).setLore(lore).setStackAmount(playersingame);
        }else if(a.estado == Arena.Estado.Reiniciando){
            return new ItemStackBuilder(Material.WOOL).setStackData((short)5).setName(name).setLore(lore).setStackAmount(playersingame);
        }else{
            return null;
        }
    }
    @Override
    public void destroy(){
        runnable.cancel();
    }
}
