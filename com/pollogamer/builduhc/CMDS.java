package com.pollogamer.builduhc;

import com.pollogamer.builduhc.arena.ArenaManager;
import com.pollogamer.builduhc.gui.ArenasMenu;
import com.pollogamer.builduhc.utils.Lang;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMDS implements CommandExecutor{

    private Principal plugin;
    public Location spawn = null;
    public Location p1 = null;
    public Location p2 = null;

    public CMDS(Principal plugin){
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String args[]) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Nel prro!");
            return true;
        }
        Player p = (Player) sender;
        if (cmd.getName().equalsIgnoreCase("arena")) {
            if (args.length == 0) {
                p.sendMessage("Utiliza /arena p1,p2,spawn y /arena create <Nombre>");
                p.sendMessage(ArenaManager.getManager().arenas.size()+" Arenas");
                return true;
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("p1")) {
                    p1 = p.getLocation();
                    p.sendMessage("Localizacion 1 marcada");
                    return true;
                } else if (args[0].equalsIgnoreCase("p2")) {
                    p2 = p.getLocation();
                    p.sendMessage("Localizacion 2 marcada");
                    return true;
                } else if (args[0].equalsIgnoreCase("create")) {
                    p.sendMessage("Utiliza /arena create <Nombre>");
                    return true;
                } else if (args[0].equalsIgnoreCase("spawn")) {
                    spawn = p.getLocation();
                    p.sendMessage("SPawn marcado");
                    return true;
                } else {
                    p.sendMessage("Argumentos Invalidos");
                    return true;
                }
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("create")) {
                    if(plugin.getMapsfile().getString("Arenas."+args[1]) != null){
                        p.sendMessage("La arena "+args[1]+" ya existe!");
                        return true;
                    }else{
                        ArenaManager.getManager().createArena(p1, p2, spawn, args[1]);
                        p.sendMessage("Arena " + args[1] + " creada");
                        return true;
                    }
                }else {
                    p.sendMessage("Arguentos invalidos");
                }

                return true;
            }else{

            }
        }
        if (cmd.getName().equalsIgnoreCase("join")) {
            if (ArenaManager.getManager().isInGame(p)) {
                p.sendMessage(Lang.alreadyingame);
                return true;
            }
            if(args.length != 1) {
                p.sendMessage("Argumentos invalidos");
                return true;
            }
            ArenaManager.getManager().addPlayer(p,args[0]);
            return true;
        }
        if (cmd.getName().equalsIgnoreCase("leave")) {
            if (ArenaManager.getManager().isInGame(p)) {
                ArenaManager.getManager().removePlayer(p);
            } else {
                p.sendMessage(Lang.noingame);
            }

            return true;
        }
        if (cmd.getName().equalsIgnoreCase("remove")) {
            if (args.length != 1) {
                p.sendMessage("Insuffcient arguments!");
                return true;
            }
            ArenaManager.getManager().removeArena(args[0]);

            return true;
        }
        if(cmd.getName().equalsIgnoreCase("arenas")){
            if (ArenaManager.getManager().isInGame(p)) {
                p.sendMessage(Lang.alreadyingame);
                return true;
            }
            new ArenasMenu(plugin,p);
            return true;
        }
        return false;
    }
}
