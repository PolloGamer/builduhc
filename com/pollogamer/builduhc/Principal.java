package com.pollogamer.builduhc;

import com.pollogamer.builduhc.arena.ArenaManager;
import com.pollogamer.builduhc.listener.GameListener;
import com.pollogamer.builduhc.listener.PlayerListener;
import com.pollogamer.builduhc.manager.ScoreboardMan;
import com.pollogamer.builduhc.stats.yml.StatsManagerYML;
import com.pollogamer.builduhc.utils.ConfigUtils;
import com.pollogamer.builduhc.utils.Items;
import com.pollogamer.builduhc.utils.Lang;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Principal extends JavaPlugin{

    public Principal plugin = this;
    public FileConfiguration config,maps,stats;

    @Override
    public void onEnable(){
        registercommands();
        new ConfigUtils(this);
        ConfigUtils.loadConfigsFile();
        new Lang(this);
        new Items(this);
        new ArenaManager(this);
        new ScoreboardMan(this);
        new PlayerListener(this);
        ArenaManager.getManager().loadGames();
        getServer().getPluginManager().registerEvents(new GameListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerListener(this),this);
        String type = plugin.getConfig().getString("TYPE");
        if(type.equalsIgnoreCase("YML")){
            Lang.statsyml = true;
            new StatsManagerYML(this);
            ConfigUtils.saveStatsfile();
        }else if (type.equalsIgnoreCase("MySQL")){
            Lang.statsyml = false;
        }else{
            plugin.setEnabled(false);
            plugin.getLogger().info("Only works with YML and MySQL");
        }
    }

    public void registercommands(){
        getCommand("join").setExecutor(new CMDS(this));
        getCommand("arena").setExecutor(new CMDS(this));
        getCommand("leave").setExecutor(new CMDS(this));
        getCommand("remove").setExecutor(new CMDS(this));
        getCommand("arenas").setExecutor(new CMDS(this));
    }


    public FileConfiguration getMapsfile(){return maps;}

    public FileConfiguration getStats(){return stats;}

    public ScoreboardMan getScoreboards(){return new ScoreboardMan(this);}
}
