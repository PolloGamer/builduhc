package com.pollogamer.builduhc.listener;

import com.pollogamer.builduhc.Principal;
import com.pollogamer.builduhc.arena.Arena;
import com.pollogamer.builduhc.arena.ArenaManager;
import com.pollogamer.builduhc.stats.StatsManager;
import com.pollogamer.builduhc.utils.Lang;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.*;

import java.util.ArrayList;
import java.util.List;

public class GameListener implements Listener{

    static Principal plugin;

    public GameListener(Principal plugin){
        this.plugin = plugin;
    }


    @EventHandler
    public void Death(PlayerDeathEvent event){
        if(!ArenaManager.getManager().isInGame(event.getEntity())) return;
        Arena arena = ArenaManager.getManager().arenas.stream().filter(a -> a.players.contains(event.getEntity().getName())).findFirst().orElse(null);
        event.setDeathMessage(null);
        event.getDrops().clear();
        List<String> winner = new ArrayList<String>();
        for (String s : arena.getPlayers()) {
            winner.add(s);
        }
        StatsManager.addLoses(event.getEntity());
        winner.remove(event.getEntity().getName());
        arena.winner = winner.get(0);
        ((CraftPlayer)event.getEntity()).getHandle().playerConnection.a(new PacketPlayInClientCommand(PacketPlayInClientCommand.EnumClientCommand.PERFORM_RESPAWN));
        event.getEntity().setGameMode(GameMode.SPECTATOR);
        event.getEntity().teleport(Bukkit.getPlayer(arena.winner).getLocation());
        StatsManager.addWin(Bukkit.getPlayer(arena.winner));
        arena.end();
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event){
        if(!ArenaManager.getManager().isInGame(event.getPlayer())) return;
        Arena arena = ArenaManager.getManager().arenas.stream().filter(a -> a.players.contains(event.getPlayer().getName())).findFirst().orElse(null);
        if(arena.estado == Arena.Estado.Esperando|| arena.estado == Arena.Estado.Empezando){
            event.setCancelled(true);
        }else{
            arena.blocktoremove.add(event.getBlock());
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event){
        if(!ArenaManager.getManager().isInGame(event.getPlayer())) return;
        Arena arena = ArenaManager.getManager().arenas.stream().filter(a -> a.players.contains(event.getPlayer().getName())).findFirst().orElse(null);
        if(arena.estado == Arena.Estado.Esperando){
            event.setCancelled(true);
            event.getPlayer().sendMessage(Lang.breakblock);
        }else{
            if(!arena.blocktoremove.contains(event.getBlock())){
                event.setCancelled(true);
                event.getPlayer().sendMessage(Lang.breakblock);
            }
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event){
        if(!ArenaManager.getManager().isInGame(event.getPlayer())) return;
        ArenaManager.getManager().removePlayer(event.getPlayer());
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event){
        if(event.getEntityType() != EntityType.PLAYER) return;
        if(!ArenaManager.getManager().isInGame((Player)event.getEntity())) return;
        Arena arena = ArenaManager.getManager().arenas.stream().filter(a -> a.players.contains(event.getEntity().getName())).findFirst().orElse(null);
        if(arena.estado != Arena.Estado.EnJuego){
            event.setCancelled(true);
        }
    }
    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event){
        if(!ArenaManager.getManager().isInGame(event.getPlayer())) return;
        if(event.getPlayer().hasPermission("builduhc.command.bypass")) return;
        event.setCancelled(true);
        event.getPlayer().sendMessage(Lang.commandsingame);
    }
    @EventHandler
    public void onDamage(EntityDamageEvent event){
        if(event.getEntityType() != EntityType.PLAYER) return;
        if(!ArenaManager.getManager().isInGame((Player)event.getEntity())) return;
        Arena arena = ArenaManager.getManager().arenas.stream().filter(a -> a.players.contains(event.getEntity().getName())).findFirst().orElse(null);
        if(arena.estado != Arena.Estado.EnJuego){
            event.setCancelled(true);
        }
    }
    @EventHandler
    public void onRegainHealt(EntityRegainHealthEvent event){
        if(event.getEntityType() != EntityType.PLAYER) return;
        if(!ArenaManager.getManager().isInGame((Player)event.getEntity())) return;
        if(event.getRegainReason() == EntityRegainHealthEvent.RegainReason.SATIATED || event.getRegainReason() == EntityRegainHealthEvent.RegainReason.REGEN){
            event.setCancelled(true);
        }
    }
    @EventHandler
    public void onCraft(CraftItemEvent event){
        if(!ArenaManager.getManager().isInGame((Player)event.getWhoClicked())) return;
        event.setCancelled(true);
        event.getWhoClicked().sendMessage(Lang.craftingame);
    }
    @EventHandler
    public void onDrop(PlayerDropItemEvent event){
        if(!ArenaManager.getManager().isInGame((Player)event.getPlayer())) return;
        event.setCancelled(true);
    }
}
