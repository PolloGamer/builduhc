package com.pollogamer.builduhc.listener;

import com.pollogamer.builduhc.Principal;
import com.pollogamer.builduhc.arena.ArenaManager;
import com.pollogamer.builduhc.manager.PlayerManager;
import com.pollogamer.builduhc.manager.ScoreboardMan;
import com.pollogamer.builduhc.utils.Lang;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerListener implements Listener{

    private Principal plugin;

    public PlayerListener(Principal plugin){
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        if(Lang.joinlobby){
            //Put scoreboard and tp to lobby
        }
        if(Lang.scoreboardinlobby){
            new BukkitRunnable(){
                public void run(){
                    if(ArenaManager.getManager().isInGame(event.getPlayer())) return;
                    if(event.getPlayer().isOnline()){
                        plugin.getScoreboards().setScoreboardLobby(event.getPlayer());
                    }else{
                        this.cancel();
                        return;
                    }
                }
            }.runTaskTimer(plugin,0,20*Lang.scoreboardrefresh);
        }
    }
    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        if(!ArenaManager.getManager().isInGame(event.getPlayer())) return;
        if(!event.hasItem()) return;
        if(!event.getItem().hasItemMeta()) return;
        if(!(event.getMaterial() == Material.getMaterial(Lang.itemleaveid))) return;
        if(event.getItem().getItemMeta().getDisplayName().equals(Lang.itemleavename)){
            Bukkit.dispatchCommand(event.getPlayer(),"leave");
        }
    }
}
