package com.pollogamer.builduhc.arena;

import com.pollogamer.builduhc.stats.StatsManager;
import com.pollogamer.builduhc.utils.Items;
import com.pollogamer.builduhc.utils.Lang;
import com.pollogamer.builduhc.manager.PlayerManager;
import com.pollogamer.builduhc.Principal;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class Arena {

    public Principal plugin;

    public Estado estado = Estado.Desactivada;
    public String nombre = null;
    public int minplayers;
    public int maxplayers;
    public String winner = null;
    public Location spawn = null;
    public Location spawnp1 = null;
    public Location spawnp2 = null;
    public int cowntdown;
    public int finalcowntdown;
    public int endingtime;
    public List<String> players = new ArrayList<String>();
    public List<Block> blocktoremove = new ArrayList<Block>();

    public Arena(Location p1,Location p2,Location spawn, String nombre,Principal plugin){
        this.minplayers = 2;
        this.maxplayers = 2;
        this.spawn = spawn;
        this.spawnp1 = p1;
        this.spawnp2 = p2;
        this.nombre = nombre;
        this.cowntdown = Lang.cooldown;
        this.finalcowntdown = Lang.cooldown;
        this.endingtime = Lang.endingcountdown;
        this.plugin = plugin;
        estado = Estado.Esperando;
    }

    public void start(){
        if(getPlayers().size() == 2){
            estado = Estado.Empezando;
            new BukkitRunnable(){
                public void run(){
                    if(cowntdown > 1){
                        if(getPlayers().size() < minplayers){
                            broadcast(Lang.nominplayers);
                            estado = Estado.Esperando;
                            cowntdown = finalcowntdown;
                            this.cancel();
                            return;
                        }
                        --cowntdown;
                        broadcast(Lang.arenastarting.replaceAll("%time%",cowntdown+""));
                    }else{
                        if(getPlayers().size() < minplayers){
                            broadcast(Lang.nominplayers);
                            estado = Estado.Esperando;
                            cowntdown = finalcowntdown;
                            this.cancel();
                            return;
                        }
                        for(String s : Lang.arenastarted){
                            s = s.replaceAll("%player1%",Bukkit.getPlayer(getPlayers().get(0)).getName());
                            s = s.replaceAll("%player2%",Bukkit.getPlayer(getPlayers().get(1)).getName());
                            broadcast(s);
                        }
                        estado = Estado.EnJuego;
                        tp();
                        this.cancel();
                        return;
                    }
                }
            }.runTaskTimer(plugin,0,20);
            new BukkitRunnable(){
                public void run(){
                    if(estado == Estado.Reiniciando){
                        ArenaManager.getManager().removeArena(nombre);
                        ArenaManager.getManager().createArena(spawnp1,spawnp2,spawn,nombre);
                        this.cancel();
                        return;
                    }
                    if(estado == Estado.EnJuego){
                        if(getPlayers().size() < minplayers){
                            if(winner==null){
                                winner = getPlayers().get(0);
                                end();
                            }
                        }
                    }
                }
            }.runTaskTimer(plugin,0,22);
        }else{
            broadcast(Lang.nominplayers);
        }
    }
    public void end(){
        for(String s : Lang.arenaend){
            s = s.replaceAll("%winner%",winner);
            broadcast(s);
        }
        new BukkitRunnable(){
            public void run(){
                if(endingtime >1){
                    if(Lang.fireworks){
                        if(Bukkit.getPlayer(winner) != null){
                            Items.spawnFirework(Bukkit.getPlayer(winner).getLocation());
                        }
                    }
                }else{
                    removeblocks();
                    if(getPlayers().size() == 2){
                        Player p1 = Bukkit.getPlayer(getPlayers().get(0));
                            ArenaManager.getManager().removePlayer(p1);
                        Player p2 = Bukkit.getPlayer(getPlayers().get(0));
                            ArenaManager.getManager().removePlayer(p2);
                        estado = Estado.Reiniciando;
                        this.cancel();
                        return;
                    }else if(getPlayers().size() == 1){
                        Player p1 = Bukkit.getPlayer(getPlayers().get(0));
                        ArenaManager.getManager().removePlayer(p1);
                        estado = Estado.Reiniciando;
                        this.cancel();
                        return;
                    }else{
                        estado = Estado.Reiniciando;
                        this.cancel();
                        return;
                    }
                }
                --endingtime;
            }
        }.runTaskTimer(plugin,0,20);
    }

    public void forcestop(){
        removeblocks();
        if(getPlayers().size() == 2){
            Player p1 = Bukkit.getPlayer(getPlayers().get(0));
            ArenaManager.getManager().removePlayer(p1);
            Player p2 = Bukkit.getPlayer(getPlayers().get(0));
            ArenaManager.getManager().removePlayer(p2);
            return;
        }else if(getPlayers().size() == 1){
            Player p1 = Bukkit.getPlayer(getPlayers().get(0));
            ArenaManager.getManager().removePlayer(p1);
            return;
        }else{
            return;
        }
    }
    public void tp(){
        Player p1 = Bukkit.getPlayer(getPlayers().get(0));
        Player p2 = Bukkit.getPlayer(getPlayers().get(1));
        p1.teleport(spawnp1);
        p2.teleport(spawnp2);
        PlayerManager.setbuildffakit(p1);
        PlayerManager.setbuildffakit(p2);
        StatsManager.addPlayed(p1);
        StatsManager.addPlayed(p2);
    }
    public void removeblocks() {
        new BukkitRunnable(){
            public void run(){
                if (blocktoremove.isEmpty()) {
                    this.cancel();
                    return;
                }
                for (Block b : blocktoremove){
                    if (blocktoremove.isEmpty()) {
                        this.cancel();
                        return;
                    }
                    b.setType(Material.AIR);
                }
            }
        }.runTaskTimer(plugin,0,0);
    }
    public enum Estado{
        Desactivada(),
        Esperando(),
        Empezando(),
        EnJuego(),
        Reiniciando()
    }

    public void broadcast(String msg){
        for(String s : this.players){
            Player p = Bukkit.getPlayer(s);
            if(p != null){
                p.sendMessage(ChatColor.translateAlternateColorCodes('&',msg));
            }
        }
    }

    public String getName(){
        return this.nombre;
    }

    public List<String> getPlayers(){
        return players;
    }

}
