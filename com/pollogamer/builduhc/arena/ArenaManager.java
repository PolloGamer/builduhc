package com.pollogamer.builduhc.arena;

import com.pollogamer.builduhc.manager.ScoreboardMan;
import com.pollogamer.builduhc.utils.ConfigUtils;
import com.pollogamer.builduhc.utils.Lang;
import com.pollogamer.builduhc.manager.PlayerManager;
import com.pollogamer.builduhc.Principal;
import com.pollogamer.builduhc.utils.Utils;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArenaManager {

    public Map<String, Location> locs = new HashMap<String, Location>();
    public static ArenaManager am = new ArenaManager();
    public Map<String, ItemStack[]> inv = new HashMap<String, ItemStack[]>();
    public Map<String, ItemStack[]> armor = new HashMap<String, ItemStack[]>();
    public List<Arena> arenas = new ArrayList<Arena>();

    static Principal plugin;

    public ArenaManager(Principal arenaPVP) {
        plugin = arenaPVP;
    }

    public ArenaManager() {

    }

    public static ArenaManager getManager() {
        return am;
    }

    public Arena getArena(String name) {
        for (Arena a : arenas) {
            if (a.getName().equalsIgnoreCase(name)) {
                return a;
            }
        }
        return null;
    }


    public void addPlayer(Player p, String name) {
        if (ArenaManager.getManager().isInGame(p)) {
            p.sendMessage(Lang.alreadyingame);
            return;
        }
        Arena a = getArena(name);
        if (a == null) {
            p.sendMessage("La arena " +name+ " no existe!");
            return;
        }
        if(a.estado != Arena.Estado.Esperando){
            p.sendMessage(Lang.arenafull);
            return;
        }
        if (a.getPlayers().size() == 0){
            p.sendMessage(Lang.joinarena.replaceAll("%arena%",a.getName()));
            PlayerManager.putallplayer(p);
            a.getPlayers().add(p.getName());
            p.teleport(a.spawn);
            String msgjoin = Lang.playerjoin;
            msgjoin = msgjoin.replaceAll("%player%",p.getName());
            msgjoin = msgjoin.replaceAll("%players%",a.getPlayers().size()+"");
            msgjoin = msgjoin.replaceAll("%maxplayers%",a.maxplayers+"");
            a.broadcast(msgjoin);
            if(Lang.scoreboardingame){
                new BukkitRunnable(){
                    public void run(){
                        if(p.isOnline()){
                            if(isInGame(p)){
                                plugin.getScoreboards().setScoreboardGame(p,a);
                            }else{
                                this.cancel();
                                return;
                            }
                        }else{
                            this.cancel();
                            return;
                        }
                    }
                }.runTaskTimer(plugin,0,20*Lang.scoreboardrefresh);
            }
        } else if (a.getPlayers().size() == 1) {
            PlayerManager.putallplayer(p);
            p.sendMessage(Lang.joinarena.replaceAll("%arena%",a.getName()));
            a.getPlayers().add(p.getName());
            p.teleport(a.spawn);
            String msgjoin = Lang.playerjoin;
            msgjoin = msgjoin.replaceAll("%player%",p.getName());
            msgjoin = msgjoin.replaceAll("%players%",a.getPlayers().size()+"");
            msgjoin = msgjoin.replaceAll("%maxplayers%",a.maxplayers+"");
            a.broadcast(msgjoin);
            if(Lang.scoreboardingame){
                new BukkitRunnable(){
                    public void run(){
                        if(isInGame(p)){
                            plugin.getScoreboards().setScoreboardGame(p,a);
                        }else{
                            this.cancel();
                            return;
                        }
                    }
                }.runTaskTimer(plugin,0,20*Lang.scoreboardrefresh);
            }
            a.start();
        }
    }

    public void removePlayer(Player p) {
        Arena a = null;
        for (Arena arena : arenas) {
            if (arena.getPlayers().contains(p.getName())) {
                a = arena;
            }
        }
        a.getPlayers().remove(p.getName());
        p.sendMessage(Lang.leftarena.replaceAll("%arena%",a.getName()));
        String msgleave = Lang.playerleave;
        msgleave = msgleave.replaceAll("%player%",p.getName());
        msgleave = msgleave.replaceAll("%players%",a.getPlayers().size()+"");
        msgleave = msgleave.replaceAll("%maxplayers%",a.maxplayers+"");
        a.broadcast(msgleave);
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        p.getInventory().setContents(inv.get(p.getName()));
        p.getInventory().setArmorContents(armor.get(p.getName()));
        inv.remove(p.getName());
        armor.remove(p.getName());
        p.teleport(locs.get(p.getName()));
        p.setGameMode(GameMode.SURVIVAL);
        locs.remove(p.getName());
        p.setFireTicks(0);
        plugin.getScoreboards().setScoreboardLobby(p);
    }

    public Arena createArena(Location p1, Location p2, Location spawn,String nombre) {
        Arena a = new Arena(p1, p2, spawn, nombre, plugin);
        arenas.add(a);
            plugin.getMapsfile().set("Arenas." + nombre + ".locs.p1", Utils.serializeLoc(p1));
            plugin.getMapsfile().set("Arenas." + nombre + ".locs.p2", Utils.serializeLoc(p2));
            plugin.getMapsfile().set("Arenas." + nombre + ".locs.spawn", Utils.serializeLoc(spawn));
            List<String> list = plugin.getMapsfile().getStringList("Arenas.Arenas");
            list.add(nombre);
            plugin.getMapsfile().set("Arenas.Arenas", list);
            ConfigUtils.saveMapsfile();
            return a;
    }

    public Arena reloadArena(Location p1, Location p2, Location spawn,String nombre) {
        Arena a = new Arena(p1, p2, spawn, nombre, plugin);
        arenas.add(a);
        return a;
    }

    public void removeArena(String nombre) {
        Arena a = getArena(nombre);
        if (a == null) {
            return;
        }
        arenas.remove(a);
        plugin.getMapsfile().set("Arenas." + nombre, null);
        List<String> list = plugin.getMapsfile().getStringList("Arenas.Arenas");
        list.remove(nombre);
        plugin.getMapsfile().set("Arenas.Arenas", list);
        ConfigUtils.saveMapsfile();
    }

    public boolean isInGame(Player p) {
        for (Arena a : arenas) {
            if (a.getPlayers().contains(p.getName()))
                return true;
        }
        return false;
    }

    public void loadGames() {
        if (plugin.getMapsfile().getStringList("Arenas.Arenas").isEmpty()) {
            return;
        }
        for (String i : plugin.getMapsfile().getStringList("Arenas.Arenas")) {
            Arena a = reloadArena(Utils.deserializeLoc(plugin.getMapsfile().getString("Arenas." + i + ".locs.p1")), Utils.deserializeLoc(plugin.getMapsfile().getString("Arenas." + i + ".locs.p2")), Utils.deserializeLoc(plugin.getMapsfile().getString("Arenas." + i + ".locs.spawn")),i);
            plugin.getLogger().info("Arena "+i+" loaded!");
        }
    }

    public String estado(Arena a){
        if(a.estado == Arena.Estado.Esperando){
            return Lang.guiarenastatewaiting;
        }else if(a.estado == Arena.Estado.Empezando){
            return Lang.guiarenastatestarting;
        }else if(a.estado == Arena.Estado.EnJuego){
            return Lang.guiarenastateingame;
        }else if(a.estado == Arena.Estado.Reiniciando){
            return "Restarting";
        }else{
            return null;
        }
    }

    public String findArena(){
        for(Arena a : this.arenas){
            if(a.estado == Arena.Estado.Esperando && a.getPlayers().size() == 0){
                return a.getName();
            }
        }
        return "";
    }
}